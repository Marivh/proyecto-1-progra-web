<?php
include_once("funcionUsuario.php");

/**
 * Inserts a new tree to the database
 *
 * @tree An associative array with the tree information
 */
function saveTree($arbol) {
  $conn = getConnection();
  $sql = "INSERT INTO arbol (`foto`, `tipo`, `edad`,`altura`)
          VALUES ('{$arbol['foto']}', '{$arbol['tipo']}', '{$arbol['edad']}', '{$arbol['altura']} cm')";
  $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}

/**
 * Metodo para anexar imagenes a la galeria de arboles
 */
function insertarImagenesGaleria($galeria) {
  $conn = getConnection();
  $sql = "INSERT INTO galeria (`id_arbol`, `foto`)
          VALUES ('{$galeria['id_arbol']}', '{$galeria['foto']}')";
  $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}



/**
 * Metodo para actualizar el arbol, cuando el amigo lo va a comprar
 *
 * @student An associative array with the student information
 */
function updateArbol($arbol) {
  $conn = getConnection();
  $sql = "UPDATE arbol set `id_propietario` = '{$arbol['id_propietario']}' , `nombre` = '{$arbol['nombre']}' , 
  `donacion` = '{$arbol['donacion']}'
    WHERE `id_arbol` = {$arbol['id_arbol']}";

  $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}

/**
 * Metodo para actualizar los detalles del arbol por parte del administrador
 */
function updateDetalles($arbol) {
  $conn = getConnection();
  $sql = "UPDATE arbol set `altura` = '{$arbol['altura']} cm' , `tipo` = '{$arbol['tipo']}' , 
  `edad` = '{$arbol['edad']}'
    WHERE `id_arbol` = {$arbol['id_arbol']}";

  $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}



/**
 * Trae todos los arboles donde el id del propietario sea nulo, estos seran los que no tienen dueno
 *
 */
function getTrees(){
  $conn = getConnection();
  $sql = "SELECT * FROM arbol where id_propietario is null";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}

/**
 *Trae de la base de datos los arboles por id del arbol
 *
 * @id Id del arbol
 */
function getTree($id){
  $conn = getConnection();
  $sql = "SELECT * FROM arbol WHERE id_arbol = $id";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result->fetch_array();
}

/**Trae de la base de datos los arboles por id del propietario */
function getMyTrees($id){
  $conn = getConnection();
  $sql = "SELECT * FROM arbol WHERE id_propietario = $id";
  $result = $conn->query($sql);
  
  $entries = array();            
  while($data=$result->fetch_assoc())
  {
      $entries[] = $data;
  }
  return $entries;
}

/**Obtiene de la base de datos los datos del arbol por amigo */
function getTreeByOwn($nombreAmigo){
  $conn = getConnection();
  $sql = " select a.id_arbol, a.foto, a.tipo, a.edad, a.altura, a.nombre from arbol as a, usuario as u
    WHERE a.id_propietario = u.id and u.nombre = '%$nombreAmigo%';";
  $result = $conn->query($sql);
  
  $entries = array();            
  while($data=$result->fetch_assoc())
  {
      $entries[] = $data;
  }
  return $entries;
}

/**Obtiene el tipo de arbol de la base de datos */
function getTreeByType(){
  $conn = getConnection();
  $sql = "SELECT DISTINCT id_arbol, tipo FROM arbol";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result->fetch_array();
}

/**
 * Elimina un arbol de la base de datps
 */
function deleteTree($id){
  $conn = getConnection();
  $sql = "DELETE FROM arbol WHERE id_arbol = $id";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}

//Metodo para obtener los datos del arbol por amigo//
function obtenerGaleriaPorArbol($idArbol){
  $conn = getConnection();
  $sql = "SELECT g.foto from arbol as a, galeria as g WHERE a.id_arbol = g.id_arbol and a.id_arbol = $idArbol";
  $result = $conn->query($sql);
  
  $entries = array();            
  while($data=$result->fetch_assoc())
  {
      $entries[] = $data;
  }
  return $entries;
}

/**
 * Muestra la informacion del arbol y del dueno de este
 */
function informacionArbolesVendidos(){
  $conn = getConnection();
  $sql = "SELECT a.id_arbol, u.nombre, u.apellido, a.tipo, a.edad, a.altura, a.foto, a.donacion from arbol as a, usuario as u
  WHERE a.id_propietario = u.id and a.id_propietario is not null";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}


/**
 * Funcion para obtener la cantidad de arboles plantados
 */
//function informacionArbolesVendidos(){
 // $conn = getConnection();
  //$sql = "SELECT COUNT(id_propietario) from arbol WHERE id_propietario is not null";
  //$result = $conn->query($sql);

  //if ($conn->connect_errno) {
    //$conn->close();
    //return [];
  //}
  //$conn->close();
 // echo $result;
//}

?>