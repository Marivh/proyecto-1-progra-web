<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous"></head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <title>Document</title>
    
</head>
<body>
    <nav class="navbar" style="background-color: #000080;" id="navbar">
    <!-- Brand -->
        <a class="navbar-brand" href="#">
            <img src="img\¡Amigos de un millón de árboles!.png" width="80" height="80" alt="">
        </a>
        <!-- Links -->
        <ul class="nav ml-auto">
            <li class="nav-item navbar-text">
                <a class="nav-link" href="#">Nosotros</a>
            </li>
            <li class="nav-item navbar-text">
                <a class="nav-link" href="registro.php">Registrarse</a>
            </li>
            <li class="nav-item navbar-text">
                <a class="nav-link" href="#elegantModalForm" data-toggle="modal" data-target="#elegantModalForm">Iniciar sesión</a>
            </li>
        </ul>
    </nav>
    </div>
    <div class="modal fade" id="elegantModalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"aria-hidden="true">
        <div class="modal-dialog" role="document">
            <!--Content-->
            <div class="modal-content form-elegant">
            <form class="form" action="login.php" method="POST" role="form">
            <!--Header-->
                <div class="modal-header text-center">
                    <h3 class="modal-title w-100 dark-grey-text font-weight-bold my-3" style="color: #191970;" id="myModalLabel"><strong>Iniciar sesión</strong></h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <!--Body-->
                <div class="modal-body">
                    <!--Body-->
                    <div class="input-group mb-2 col-md-12">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="addon-wrapping"><i class="fas fa-envelope-square"></i></span>
                        </div>
                        <input class="form-control validate white-text" type="type" name = "correo" placeholder="Correo">
                    </div>
                    <div class="input-group mb-2 col-md-12">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="addon-wrapping"><i class="fas fa-lock"></i></span>
                        </div>
                        <input class="form-control validate white-text" type="password" name = "contrasena" placeholder="Contraseña">
                    </div>                    
                </div>
                <!--Footer-->
                <div class="modal-footer mx-5 pt-3 mb-1">
                    <div class="form-group col-md-12 text-center">
                            <button class="btn btn-success my-2 my-sm-0" type="submit">Iniciar sesión</button>
                        </div>
                    <div class="form-group col-md-12 text-center">
                    <p class="font-small grey-text  text-center">No tiene una cuenta? <a href="registro.php" class="blue-text ml-1">
                        Regístrese</a></p>
                        </div> 
                </div>
            </form>
        </div>
</body>
</html>