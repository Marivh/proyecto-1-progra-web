<?php
  include_once('funcionArbol.php');


  if($_REQUEST['id_arbol']) {
    $arbol = getTree($_REQUEST['id_arbol']);
  }

  // if editing
  if($_POST){
      $arbol['altura'] = $_POST['altura'];
      $arbol['tipo'] = $_POST['tipo'];
      $arbol['edad'] = $_POST['edad'];
      updateDetalles($arbol);
    if ($filename = uploadPicture('picture')){
        $galeria['id_arbol'] = $arbol['id_arbol'];
        $galeria['foto'] = $filename;
        insertarImagenesGaleria($galeria);
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" type="text/css" href="detalleArbol.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous"></head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

  <title>Document</title>
</head>
<body>
<div class="container">
    <form   method="POST" class="form" role="form" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $user['id']?>">
        <br>
        <h1 class="text-center" style="color: #191979;" >Editar árbol</h1>
        <div class="row">
            <!--Carga los input con los valores del arbol-->
            <div class="col bg-white border-right">
                <div class="form-group col-md-12">
                    <br>
                    <label class="sr-only" for="">Tipo</label>
                    <input type="text" class="form-control" id="tipo" name="tipo" placeholder="Tipo" value="<?php echo $arbol['tipo'] ?>">
                </div>
                <div class="form-group col-md-12">
                    <label class="sr-only" for="">Edad</label>
                    <input type="text" class="form-control" id="" name="edad" placeholder="Edad" value="<?php echo $arbol['edad'] ?>">
                </div>
                <div class="input-group col-md-12">
                    <label class="sr-only" for="">Altura</label>
                    <input type="text" class="form-control" id="" name="altura" placeholder="Altura" value="<?php echo $arbol['altura'] ?>">
                    <div class="input-group-append">
                        <span class="input-group-text" id="addon-wrapping">cm</span>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <br>
                <img src="<?php echo $arbol['foto']?>" width="300" height="300" class="col-md-12"></img>
                </div>
            </div>
            <div class="col bg-white">
                <!--Muestra la galeria de arboles y da la opcion para agregar mas fotos-->
                <div class="justify-content-center">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body col-lg-12 col-md-4 col-6">
                            <?php
                            $trees = obtenerGaleriaPorArbol($arbol['id_arbol']);
                            $treesHtml = "";
                            foreach ($trees as $tree) {
                                $treesHtml .= "<img class='img-fluid' img src={$tree['foto']} width='150' height='150'>"
                                ;}
                                echo $treesHtml;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-12 text-center">
                    <br>
                <input type="file" name="picture" id="picture" class="form-control-file col-md-12">
                </div>
            </div>
        </div>
        <div class="col-md-12 text-center">
            <br>
                <a href="vistaAdministrador.php" class="btn btn-primary btn-md">Atrás</a>
                <button class="btn btn-primary btn-md">Actualizar</button>
        </div>
    </form>
</div>
</body>
</html>