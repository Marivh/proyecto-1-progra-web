<?php


/**
 *  Gets a new mysql connection
 */
function getConnection() {
  $connection = new mysqli('localhost:3306', 'root', '', 'proyectoweb');
  if ($connection->connect_errno) {
    printf("Connect failed: %s\n", $connection->connect_error);
    die;
  }
  return $connection;
}

/**
 * Inserts a new student to the database
 *
 * @student An associative array with the student information
 */
function saveUser($user) {
  $conn = getConnection();
  $sql = "INSERT INTO usuario ( `nombre`, `apellido`, `pais`, `telefono`, `direccion`, `correo`, `contrasena`, `tipo`, `foto`)
  VALUES ('{$user['nombre']}', '{$user['apellido']}', '{$user['pais']}' , '{$user['telefono']}',
  '{$user['direccion']}', '{$user['correo']}', '{$user['contrasena']}', 'usuario', '{$user['foto']}')"
;
  $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}


/**
 * Get all students from the database
 *
 */
function getUsers(){
  $conn = getConnection();
  $sql = "SELECT * FROM usuario WHERE tipo = 'usuario'";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}

/**
 * Get one specific student from the database
 *
 * @id Id of the student
 */
function getUser($id){
  $conn = getConnection();
  $sql = "SELECT * FROM usuario WHERE id = $id";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result->fetch_array();
}

/**
 * Deletes an student from the database
 */
function deleteUser($id){
  $conn = getConnection();
  $sql = "DELETE FROM usuario WHERE id = $id";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}

/**
 * Uploads an image to the server
 *
 * @inputName name of the input that holds the image in the request
 */
function uploadPicture($inputName){
  $fileObject = $_FILES[$inputName];

  $target_dir = "update/";
  $target_file = $target_dir . basename($fileObject["name"]);
  $uploadOk = 0;
  if (move_uploaded_file($fileObject["tmp_name"], $target_file)) {
    return $target_file;
  } else {
    return false;
  }
}

/**
 * Get one specific user from the database
 *
 * @id Id of the user
 */
function authenticate($email, $password){
  $conn = getConnection();
  $sql = "SELECT * FROM usuario WHERE `correo` = '$email' AND `contrasena` = '$password'";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return $result->fetch_array();
}

function obtenerAmigoPorNombre(){
    if(isset($_GET)){
    $busqueda = trim($_GET['buscar']);
    $entero = 0;
    if (empty($busqueda)){
      $texto = "Búsqueda sin resultados";
    }else{
      $conn = getConnection();
      $sql = "SELECT * FROM usuario WHERE nombre LIKE '%".$busqueda."%' OR 
      apellido LIKE '%".$busqueda."%'";
      $result = $conn->query($sql);
      
      $entries = array();            
      while($data=$result->fetch_assoc())
      {
          $entries[] = $data;
      }
      return $entries;
    }
  }
}