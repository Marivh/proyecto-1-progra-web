<?php
  include_once('funcionArbol.php');

  if($_REQUEST['id']) {
    $arbol = getMyTrees($_REQUEST['id']);
  }

  // if editing
  //if($_POST){
      //now that we upload we can save the student
    //  $arbol['id_propietario'] = $user['id'];
      //$arbol['nombre'] = $_POST['nombreArbol'];
      //$arbol['donacion'] = $_POST['donacion'];
      //updateArbol($arbol);
  //}


  // else {
  //   // header('Location: /crud/?status=error');
  // }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" type="text/css" href="detalleArbol.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous"></head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

  <title>Document</title>
</head>
<body>
<div class="container">
<h1 class="font-weight-light text-center text-lg-left mt-4 mb-0">Árboles del usuario</h1>
               <div class="row  h-100 justify-content-start">
                   <div class="col-sm-12">
                       <div class="card justify-content-start">
                           <div class="card-body justify-content-start">
                               <?php
                                    $id = $_REQUEST['id'];
                                    if($id){
                                        $trees = getMyTrees($id);
                                        $treesHtml = "";
                                        foreach ($trees as $tree) {
                                            $treesHtml .= "<img id_arbol='{$tree['id_arbol']}' class='img-fluid' img src={$tree['foto']} width='150' height='150'>
                                            <h3>{$tree['nombre']}</h3><p><a href='editarArbol.php?id_arbol={$tree['id_arbol']}'>Editar árbol</a></p>"
                                            ;}
                                        echo $treesHtml;
                                    }
                               ?>
                           </div>
                       </div>
                   </div>
            </div>
</div>
</body>
</html>