<?php
  include_once('funcionArbol.php');

  session_start();


  $user = $_SESSION['usuario'];
  if (!$user) {
    header('Location: index.php');
  }

  if($_REQUEST['id_arbol']) {
    $arbol = getTree($_REQUEST['id_arbol']);
  }

  // if editing
  if($_POST){
      //now that we upload we can save the student
      $arbol['id_propietario'] = $user['id'];
      $arbol['nombre'] = $_POST['nombreArbol'];
      $arbol['donacion'] = $_POST['donacion'];
      updateArbol($arbol);
  }


  // else {
  //   // header('Location: /crud/?status=error');
  // }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" type="text/css" href="detalleArbol.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous"></head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <script src="https://www.paypal.com/sdk/js?client-id=sb"></script>

  <title>Document</title>
</head>
<body>
<div class="container">
    <form   method="POST" class="form" role="form" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $user['id']?>">
      <div class="row">
        <div class="col bg-white border-right">
          <div class="form-group col-md-12">
            <br>
            <h1 class="text-center" style="color: #191979;" >Detalles del árbol</h1>
            <label class="sr-only" for="">Tipo</label>
            <h4>Tipo:</h4>
            <input type="text" class="form-control" id="tipo" name="tipo" placeholder="Tipo" value="<?php echo $arbol['tipo'] ?>">
          </div>
          <div class="form-group col-md-12">
          <h4>Edad:</h4>
            <label class="sr-only" for="">Edad</label>
            <input type="text" class="form-control" id="" name="edad" placeholder="Edad" value="<?php echo $arbol['edad'] ?>">
          </div>
          <div class="form-group col-md-12">
          <h4>Altura:</h4>
            <label class="sr-only" for="">Altura</label>
            <input type="text" class="form-control" id="" name="altura" placeholder="Altura" value="<?php echo $arbol['altura'] ?>">
          </div>
          <div class="form-group col-md-12 text-center">
            <img src="<?php echo $arbol['foto']?>" width="350" height="350" class="col-md-12"></img>
          </div>
        </div>
        <div class="col bg-white border-0">
          <br>
          <div class="form-group col-md-12">
          <h1 class="text-center" style="color: #191979;">Comprar árbol</h1>
          <br>
          <br>
            <label class="sr-only" for="">Nombre</label>
            <input type="text" class="form-control" id="nombreArbol" name="nombreArbol" placeholder="Elige un nombre para tu arbol">
          </div>
          <div class="input-group mb-2 col-md-12">
            <div class="input-group-prepend">
              <div class="input-group-text">$</div>
            </div>
            <label class="sr-only" for="">Donacion</label>
            <input type="text" class="form-control" id="" name="donacion" placeholder="Monto a donar">
          </div> 
        </div>
      </div>
      <div class="row">
        <div class="col text-right">
          <a href="vistaUsuario.php" class="btn btn-primary btn-md">Atrás</a>
        </div>
        <div class="col text-left">
          <button type="submit" class="btn btn-primary">Adquirir</button>
        </div>  
          <div class="form-group col-md-12 text-center">
            <script class="text-center">paypal.Buttons().render('body');</script>
          </div> 
      </div>
    </form>
  </div>
</body>
</html>