<?php
  include_once('funcionUsuario.php');

  /**Valida que los campos no esten vacios y si no estan vacios registra al usuario */
  if($_POST){
    if(empty($_POST['nombre'])){
      echo "Apellido requerido";
    }elseif(empty($_POST['apellido'])){
      echo "Apellido requerido";
    }elseif(empty($_POST['pais'])){
      echo "Pais requerido";
    }elseif(empty($_POST['direccion'])){
      echo "Dirección requerida";
    }elseif(empty($_POST['correo'])){
      echo "Correo requerido";
    }elseif(empty($_POST['telefono'])){
      echo "Teléfono requerido";
    }elseif(empty($_POST['contrasena'])){
      echo "Contraseña requerida";
    }else{
    if ($filename = uploadPicture('picture')){
      //now that we upload we can save the student
      $user['nombre'] = $_POST['nombre'];
      $user['apellido'] = $_POST['apellido'];
      $user['pais'] = $_POST['pais'];
      $user['direccion'] = $_POST['direccion'];
      $user['foto'] = $filename;
      $user['correo'] = $_POST['correo'];
      $user['telefono'] = $_POST['telefono'];
      $user['contrasena'] = $_POST['contrasena'];
      saveUser($user);
      header('Location: index.php');
    } else {
      echo "There was an error saving the picture";
    }
  }
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" type="text/css" href="registro.css">
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="assets/js/actions.js"></script>
  <title>Document</title>
</head>
<body>
<div class="container">
    <form method="POST" class="form" role="form" enctype="multipart/form-data" onsubmit="return validateUserForm();">
    <br>
    <br>
    <br>
    <!--Formulario para registrar al usuario-->
    <div class="form-group col-md-6">
        <img src="img\cierreagr2.jpg" alt="">
      </div>
      <div class="form-group col-md-6">
      <h1>Registrarse</h1>
      </div>
      <div class="form-group col-md-6">
        <label class="sr-only" for="">Nombre</label>
        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre">
      </div>
      <div class="form-group col-md-6">
        <label class="sr-only" for="">Apellido</label>
        <input type="text" class="form-control" id="" name="apellido" placeholder="Apellido">
      </div>
      <div class="form-group col-md-6">
        <label class="sr-only" for="">País</label>
        <input type="text" class="form-control" id="" name="pais" placeholder="País">
      </div>
      <div class="form-group col-md-6">
        <label class="sr-only" for="">Dirección</label>
        <input type="" class="form-control" id="" name="direccion" placeholder="Dirección">
      </div>
      <div class="form-group col-md-6">
        <label class="sr-only" for="">Correo</label>
        <input type="text" class="form-control" id="" name="correo" placeholder="Correo">
      </div>
      <div class="form-group col-md-6">
        <label class="sr-only" for="">Teléfono</label>
        <input type="text" class="form-control" id="" name="telefono" placeholder="Teléfono">
      </div>
      <div class="form-group col-md-6">
        <label class="sr-only" for="">Contraseña</label>
        <input type="password" class="form-control" id="" name="contrasena" placeholder="Contraseña">
      </div>
      <input type="file" name="picture" id="picture" class="form-control-file col-md-6">
      <div class="form-group col-md-6">
      <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
      
    </form>
</div>

</body>
</html>

