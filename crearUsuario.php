<?php

  if(isset($_POST['nombre']) && isset($_POST['apellido']) && isset($_POST['pais']) && isset($_POST['direccion']) 
  && isset($_POST['telefono']) && isset($_POST['correo']) && isset($_POST['contrasena']) && isset($_POST['foto']) ) {
    $saved = saveUser($_POST);

    if($saved) {
      header('Location: index.php');
    } else {
      header('Location: registro.php/?status=error');
    }
  } else {
    header('Location: registro.php/?status=error');
  }
