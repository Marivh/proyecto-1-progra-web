<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
include_once('funcionUsuario.php');
require_once('funcionArbol.php');

// if editing
if($_POST){
 if ($filename = uploadPicture('picture')){
   //now that we upload we can save the student
   $arbol['tipo'] = $_POST['tipo'];
   $arbol['edad'] = $_POST['edad'];
   $arbol['altura'] = $_POST['altura'];
   $arbol['foto'] = $filename;
   saveTree($arbol);
 } else {
   echo "Ha ocurrido un error al agregar la imagen";
 }
}


session_start();
$user = $_SESSION['usuario'];
  if (!$user) {
    header('Location: index.php');
  }
  ?>

  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      
      <link rel="stylesheet" type="text/css" href="vistaUsuario.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
      <title>Document</title>
  </head>
  <body>
        <nav class="navbar" style="background-color: #000080;">
            <!-- Brand -->
            <a class="navbar-brand" href="#">
                <img src="img\¡Amigos de un millón de árboles!.png" width="80" height="80" alt="">
            </a>
            <!-- Links -->
            <ul class="nav ml-auto">
                <li class="nav-item border-right">
                    <a class="nav-link" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Cantidad de amigos registrados</a>
                </li>
                <li class="nav-item border-right">
                    <a class="nav-link"  data-toggle="collapse" href="#arbolesVendidos" aria-expanded="false" aria-controls="arbolesVendidos">Cantidad de árboles plantados</a>
                </li>
                <li class="nav-item border-right">
                <a class="nav-link" data-toggle="collapse" href="#registro" aria-expanded="false" aria-controls="registro">Registrar árbol</a>
                </li>
                <li class="nav-item border-right">
                    <a class="nav-link" data-toggle="collapse" href="#busquedaA" aria-expanded="false" aria-controls="busquedaA">Administrar árboles</a>
                </li>
                <!--Ventana de perfil-->
                <li class="nav-item dropdown navbar-text">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="far fa-user"></span> 
                        <strong><?php echo $user['nombre'] ?></strong>
                        <span class="glyphicon glyphicon-chevron-down"></span>
                    </a>
                    <div class="container">
                    <ul class="dropdown-menu ml-auto">
                        <li id="login">
                            <div class="navbar-login">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <p class="text-center">
                                            <span><img src="<?php echo $user['foto']?>" width="80" height="80"></img></span>
                                        </p>
                                    </div>
                                    <div class="col-lg-8">
                                        <p class="text-left"><strong><?php echo $user['nombre'] ?></strong></p>
                                        <p class="text-left small"><?php echo $user['correo'] ?></p>
                                        <p class="text-left">
                                            <a href="logout.php" class="btn btn-primary btn-block btn-sm">Cerrar sesión</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="navbar-login navbar-login-session">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <p>
                                        	<a href="#" class="btn btn-primary btn-block">Mi perfil</a>
                                            <a href="#" class="btn btn-danger btn-block">Cambiar contraseña</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    </div>
                </li>
            </ul>
        </nav>

        <!--Muestra la tabla de los amigos registrados-->
        <div class="collapse" id="collapseExample">
            <div class="card card-body table-responsive">
                <table class="table table-light table-hover table-striped">
                    <tbody>
                        <tr class="table-success text-center">
                            <td scope="col">Id</td>
                            <td scope="col">Nombre</td>
                            <td scope="col">Apellido</td>
                            <td scope="col">País</td>
                            <td scope="col">Dirección</td>
                            <td scope="col">Correo</td>
                            <td scope="col">Teléfono</td>
                        </tr>
                <?php
                $users = getUsers();
                $usersHtml = "";
                foreach ($users as $user) {
                    $usersHtml .= "<tr class='text-center' id='usuario_{$user['id']}'><td>{$user['id']}</td><td>{$user['nombre']}</td>
                    <td>{$user['apellido']}</td><td>{$user['pais']}</td><td>{$user['direccion']}</td><td>{$user['correo']}</td>
                    <td>{$user['telefono']}</td></tr>";
                }
                echo $usersHtml;
                ?>
                    </tbody>
                </table>  
            </div>
        </div> 
        
        <!--Muestra el formulario de registro de arboles-->
        <div class="collapse" id="registro">
            <div class="card card-body">
                    <form class="form" method="POST" role="form" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col text-center">
                                <img src="img\cierreagr2.jpg" alt="">
                            </div>
                            <div class="col">
                                <br>
                                <br>
                                <br>
                                <div class="form-group col-md-12 text-center">
                                <h1>Registrar árbol</h1>
                                </div>
                                <div class="form-group col-md-12 text-center">
                                    <label class="sr-only" for="">Tipo</label>
                                    <input type="text" class="form-control" id="tipo" name="tipo" placeholder="Tipo">
                                </div>
                                <div class="form-group col-md-12 text-center">
                                    <label class="sr-only" for="">Edad</label>
                                    <input type="text" class="form-control" id="" name="edad" placeholder="Edad">
                                </div>
                                <div class="input-group mb-2 col-md-12">
                                    <input class="form-control" type="text" name = "altura" placeholder="Altura">
                                    <div class="input-group-append">
                                    <span class="input-group-text" id="addon-wrapping">cm</span>
                                    </div>
                                </div> 
                                <div class="form-group col-md-12">
                                <input type="file" name="picture" id="picture" class="form-control-file">
                                </div>
                                <div class="form-group col-md-12 text-center">
                                <button type="submit" class="btn btn-primary md">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </form>
            </div>
        </div>

        <!--Muestra las opciones del nombre que el administrador ingreso para ver los arboles-->
        <div class="collapse" id="busquedaA">
            <div class="card card-body">
                <form id="buscador" name="buscador" method="GET" action="<?php echo $_SERVER['PHP_SELF'] ?>">
                    <input id="buscar" name="buscar" type="search" placeholder="Buscar aquí…" autofocus onkeyup="obtenerAmigoPorNombre();">
                    <input type="submit" name="buscador" class="btn btn-primary md" value="buscar">
                </form>
                <br>
                <section id="tabla_resultado">
                    <table class="table table-light table-hover table-striped">
                        <tbody>
                            <tr class="table-success text-center">
                                <td scope="col">Id</td>
                                <td scope="col">Nombre</td>
                                <td scope="col">Apellido</td>
                                <td scope="col">País</td>
                                <td scope="col">Dirección</td>
                                <td scope="col">Correo</td>
                                <td scope="col">Teléfono</td>
                                <td scope="col">Acciones</td>
                            </tr>
                    <?php
                        $users = obtenerAmigoPorNombre();
                        $usersHtml = "";
                        if (is_array($users) || is_object($user)){
                        foreach ($users as $user) {
                            $usersHtml .= "<tr class='text-center' id='student_{$user['id']}'><td>{$user['id']}</td><td>{$user['nombre']}</td>
                            <td>{$user['apellido']}</td><td>{$user['pais']}</td><td>{$user['direccion']}</td><td>{$user['correo']}</td>
                            <td>{$user['telefono']}</td>
                            <td> <a href='vistaAbolesPorAmigo.php?id={$user['id']}'>Ver arboles</a>";
                        }
                        echo $usersHtml;
                    }
                    ?>
                        </tbody>
                    </table>  
                </section>
            </div>
        </div>

        <div class="collapse" id="arbolesVendidos">
            <div class="card card-body table-responsive">
                <table class="table table-light table-hover table-striped">
                    <tbody>
                        <tr class="table-success text-center">
                            <td scope="col">Id</td>
                            <td scope="col">Nombre</td>
                            <td scope="col">Apellido</td>
                            <td scope="col">Edad del árbol</td>
                            <td scope="col">Altura</td>
                            <td scope="col">Especie</td>
                            <td scope="col">Donación</td>
                        </tr>
                <?php
                $arboles = informacionArbolesVendidos();
                $arbolesHtml = "";
                foreach ($arboles as $arbol) {
                    $usersHtml .= "<tr class='text-center' id='arbol_{$arbol['id_arbol']}'><td>{$arbol['id_arbol']}</td><td>{$arbol['nombre']}</td>
                    <td>{$arbol['apellido']}</td><td>{$arbol['edad']}</td><td>{$arbol['altura']}</td><td>{$arbol['tipo']}</td>
                    <td>{$arbol['donacion']}</td></tr>";
                }
                echo $usersHtml;
                ?>
                    </tbody>
                </table>  
            </div>
        </div> 
  </body>
  </html>