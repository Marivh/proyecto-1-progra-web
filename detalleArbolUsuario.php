<?php
  include_once('funcionArbol.php');

  session_start();


  $user = $_SESSION['usuario'];
  if (!$user) {
    header('Location: index.php');
  }

  if($_REQUEST['id_arbol']) {
    $arbol = getTree($_REQUEST['id_arbol']);
  }

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" type="text/css" href="detalleArbol.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous"></head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

  <title>Document</title>
</head>
<body>
<div class="container">
    <form   method="POST" class="form" role="form" enctype="multipart/form-data">
      <input type="hidden" name="id" value="<?php echo $user['id']?>">
      <br>
      <h1 class="text-center" style="color: #191979;" >Detalles del árbol</h1>
        <div class="row">
          <div class="col bg-white border-right">
            <div class="form-group col-md-12">
              <br>
              <h4>Tipo:</h4>
              <input type="text" class="form-control" id="tipo" readonly name="tipo" placeholder="Tipo" value="<?php echo $arbol['tipo'] ?>">
            </div>
            <div class="form-group col-md-12">
            <h4>Nombre:</h4>
              <label class="sr-only" for="">Nombre</label>
              <input type="text" class="form-control" id="" readonly name="nombre" placeholder="Nombre" value="<?php echo $arbol['nombre'] ?>">
            </div>
            <div class="form-group col-md-12">
            <h4>Edad:</h4>
              <label class="sr-only" for="">Edad</label>
              <input type="text" class="form-control" id="" readonly name="edad" placeholder="Edad" value="<?php echo $arbol['edad'] ?>">
            </div>
            <div class="form-group col-md-12">
            <h4>Altura:</h4>
              <label class="sr-only" for="">Altura</label>
              <input type="text" class="form-control" id="" readonly name="altura" placeholder="Altura" value="<?php echo $arbol['altura'] ?>">
            </div>
          </div>
          <div class="col bg-white">
            <div class="form-group col-md-12 text-center">
              <br>
              <img src="<?php echo $arbol['foto']?>" width="350" height="350" class="col-md-12"></img>
              <div class="justify-content-center">
                <div class="col-xl-12">
                  <div class="card" style="width: 100%">
                        <div class="card-body" id="prueba">
                        <?php
                        $trees = obtenerGaleriaPorArbol($arbol['id_arbol']);
                        $treesHtml = "";
                        foreach ($trees as $tree) {
                            $treesHtml .= "<img  class='img-fluid' img src={$tree['foto']} width='150' height='150'>"
                            ;}
                            echo $treesHtml;
                            ?>
                        </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12 text-center">
              <br>
              <a href="vistaUsuario.php" class="btn btn-primary btn-xl">Atrás</a>
            </div>
        </div>
    </form>
</div>
</body>
</html>