<?php
include_once('funcionUsuario.php');
include_once('funcionArbol.php');
  session_start();
  $user = $_SESSION['usuario'];
  if (!$user) {
    header('Location: index.php');
  }
  ?>

  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" type="text/css" href="vistaUsuario.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous"></head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
      <title>Document</title>
  </head>
  <body>
  <nav class="navbar" style="background-color: #000080;">
            <!-- Brand -->
            <a class="navbar-brand" href="#">
                <img src="img\Amigos de un millón de árboles.png" width="80" height="80" alt="">
            </a>
            <!-- Links -->
            <ul class="nav ml-auto">
                <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#mis-arboles" aria-expanded="false" aria-controls="mis-arboles">Mis árboles</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#arbol" aria-expanded="false" aria-controls="arbol">Comprar árbol</a>
                </li>
                <!--Ventana de pergfil-->
                <li class="nav-item dropdown navbar-text">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="far fa-user"></span> 
                        <strong><?php echo $user['nombre'] ?></strong>
                        <span class="glyphicon glyphicon-chevron-down"></span>
                    </a>
                    <div class="container">
                    <ul class="dropdown-menu ml-auto">
                        <li id="login">
                            <div class="navbar-login">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <p class="text-center">
                                            <span><img src="<?php echo $user['foto']?>" width="80" height="80"></img></span>
                                        </p>
                                    </div>
                                    <div class="col-lg-8">
                                        <p class="text-left"><strong><?php echo $user['nombre'] ?></strong></p>
                                        <p class="text-left small"><?php echo $user['correo'] ?></p>
                                        <p class="text-left">
                                            <a href="logout.php" class="btn btn-primary btn-block btn-sm">Cerrar sesión</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="navbar-login navbar-login-session">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <p>
                                        	<a href="#" class="btn btn-primary btn-block">Mi perfil</a>
                                            <a href="#" class="btn btn-danger btn-block">Cambiar contraseña</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    </div>
                </li>
            </ul>
        </nav>
        <!--Muestra los arboles comprados por este usuario-->
        <div class="collapse" id="mis-arboles">
            <div class="card card-body table-responsive">
            <h1 class="font-weight-light text-center text-lg-left mt-4 mb-0">Mis arboles</h1>
               <div class="row  h-100 justify-content-start">
                   <div class="col-sm-12">
                       <div class="card justify-content-start">
                           <div class="card-body justify-content-start">
                               <?php
                                    $id = $user['id'];
                                    if($id){
                                        $trees = getMyTrees($id);
                                        $treesHtml = "";
                                        foreach ($trees as $tree) {
                                            $treesHtml .= "<img id_arbol='{$tree['id_arbol']}' class='img-fluid' img src={$tree['foto']} width='150' height='150'>
                                            <h3>{$tree['nombre']}</h3><p><a href='detalleArbolUsuario.php?id_arbol={$tree['id_arbol']}'>Ver detalle</a></p>"
                                            ;}
                                        echo $treesHtml;
                                    }
                               ?>
                           </div>
                       </div>
                   </div>
               </div>
            </div>
        </div> 

        <!--Muestra los arboles disponibles para comprar-->
        <div class="collapse" id="arbol">
            <div class="card card-body h-100 justify-content-start">
                <form action="">
                    <h3></h3>
                </form>
                <h1 class="font-weight-light text-center text-lg-left mt-4 mb-0">Galería de arboles</h1>
               
                <div class="row  h-100 justify-content-start">
                    <div class="col-sm-12">
                        <div class="card justify-content-start">
                            <div class="card-body justify-content-start">
                                <?php
                                    $trees = getTrees();
                                    $treesHtml = "";
                                    foreach ($trees as $tree) {
                                        $treesHtml .= "<img id_arbol='{$tree['id_arbol']}' class='img-fluid' img src={$tree['foto']} width='150' height='150'>
                                        <h3>{$tree['tipo']}</h3><p><a href='detalleArbol.php?id_arbol={$tree['id_arbol']}'>Ver detalle</a></p>"
                                        ;}
                                    echo $treesHtml;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </body>
  </html>



  

